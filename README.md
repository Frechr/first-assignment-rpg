# First Assignment RPG



## Description
This is my solution for the first assignment in the .NETRemoteJan23 course. It utilizes inheritance, tests and some other goodies to achieve the goal.

## Usage
You can create heroes like this:  
`Hero hero = new Warrior()`  
And then you can create equipment for your hero to wear like this:  
```
Armor armor = new Armor(<armor name>,<required level>,<armor slot>, <armor type>,<armor attributes>);  
Weapon weapon = new Weapon(<weapon name>,<required level>,<weapon type>,<weapon damage>);  
```

Then equip your hero like this:  
``` 
hero.Equip(armor);  
hero.Equip(weapon);  
```

There are also helper methods in the weapon and armor classes to create equipment faster:  
```
Weapon axe = Weapon.CreateAxe();  
Armor helmet = Armor.CreateHelmet();  
```

If you want to create a full loadout there is a helper method for that in the HeroEquipment class (currently warrior-only):  
`hero.Equipment = HeroEquipment.CreateWarriorBeginnerLoadout();`  

For displaying your hero's properties you can call the following method:  
`hero.Display();`  

That's about it!  

## Author
Fredrik Christensen 😊