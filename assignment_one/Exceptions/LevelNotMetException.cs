﻿using System.Runtime.Serialization;

namespace FirstAssignment_RPG.Exceptions
{
    [Serializable]
    public class LevelNotMetException : Exception
    {
        public LevelNotMetException(string? message) : base(message)
        {
        }
    }
}