﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    //Base class for each classes' level attributes.
    public abstract class LevelAttributes : IEquatable<LevelAttributes>
    {
        //CurrentAttributes is the attributes which the class begins with
        protected HeroAttribute CurrAttributes { get; set; }
        //ClassLevelAttributes is the attributes which the CurrAttributes will be increased by upon leveling up
        protected HeroAttribute ClassLevelAttributes { get; set; }

        protected LevelAttributes(HeroAttribute currAttributes, HeroAttribute classLevelAttributes)
        {
            CurrAttributes = currAttributes;
            ClassLevelAttributes = classLevelAttributes;
        }

        public void IncreaseStats()
        {
            CurrAttributes.Add(ClassLevelAttributes);
        }

        public HeroAttribute GetCurrentAttributes()
        {
            return CurrAttributes;
        }

        public HeroAttribute GetLevelAttributes()
        {
            return ClassLevelAttributes;
        }

        public bool Equals(LevelAttributes? other)
        {
            try
            {
                return CurrAttributes.Equals(other.CurrAttributes) && ClassLevelAttributes.Equals(other.ClassLevelAttributes);
            }
            catch (ArgumentNullException)
            {
                return false;
            }
        }
    }
}
