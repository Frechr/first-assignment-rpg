﻿using FirstAssignment_RPG.Hero;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    public class HeroAttribute : IEquatable<HeroAttribute>
    {
        public static HeroAttribute DEFAULT = new HeroAttribute(0, 0, 0);
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public void Add(HeroAttribute heroAttribute)
        {
            Strength += heroAttribute.Strength;
            Dexterity += heroAttribute.Dexterity;
            Intelligence += heroAttribute.Intelligence;
        }

        public bool Equals(HeroAttribute? other)
        {
            try
            {
                return Strength == other.Strength && Dexterity == other.Dexterity && Intelligence == other.Intelligence;
            }
            catch (ArgumentNullException)
            {
                return false;
            }
        }

        //Methods for easier construction of attributes, note that I multiply the HeroAttribute
        //with integers, that's possible because of operator overloading which can be seen at the bottom.
        public static HeroAttribute CreateBeginnerArmorAttributes<T>() where T : Hero.Hero, new()
        {
            Hero.Hero hero = new T();
            var beginnerAttributes = hero.LevelAttributes.GetLevelAttributes();
            return beginnerAttributes;
        }
        public static HeroAttribute CreateIntermediateArmorAttributes<T>() where T : Hero.Hero, new()
        {
            Hero.Hero hero = new T();
            var beginnerAttributes = hero.LevelAttributes.GetLevelAttributes() * 10;
            return beginnerAttributes;
        }
        public static HeroAttribute CreateAdvancedArmorAttributes<T>() where T : Hero.Hero, new()
        {
            Hero.Hero hero = new T();
            var beginnerAttributes = hero.LevelAttributes.GetLevelAttributes() * 100;
            return beginnerAttributes;
        }
        //Operator overloading such that HeroAttributes can be multiplied more easily
        public static HeroAttribute operator *(HeroAttribute attributes, int multiplier) => new HeroAttribute(attributes.Strength * multiplier, attributes.Dexterity * multiplier, attributes.Intelligence * multiplier);
    }
}
