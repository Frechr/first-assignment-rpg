﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    public class RangerLevelAttributes : LevelAttributes
    {
        public RangerLevelAttributes() : base(new HeroAttribute(1, 7, 1), new HeroAttribute(1, 5, 1))
        {
        }
    }
}
