﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    public class WarriorLevelAttributes : LevelAttributes
    {
        public WarriorLevelAttributes() : base(new HeroAttribute(5, 2, 1), new HeroAttribute(3, 2, 1))
        {
        }
    }
}
