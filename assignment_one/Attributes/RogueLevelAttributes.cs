﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    public class RogueLevelAttributes : LevelAttributes
    {
        public RogueLevelAttributes() : base(new HeroAttribute(2, 6, 1), new HeroAttribute(1, 4, 1))
        {
        }
    }
}
