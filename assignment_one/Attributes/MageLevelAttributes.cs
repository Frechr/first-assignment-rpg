﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Attributes
{
    public class MageLevelAttributes : LevelAttributes
    {
        public MageLevelAttributes() : base(new HeroAttribute(1, 1, 8), new HeroAttribute(1, 1, 5))
        {
        }
    }
}
