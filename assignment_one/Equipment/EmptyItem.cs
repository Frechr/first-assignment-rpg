﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Equipment
{
    //I don't like handling nulls so I use this to denote an empty slot in the hero's equipment
    public class EmptyItem : Item
    {
        public EmptyItem() : base("EMPTY_ITEM", 0, Slot.DEFAULT)
        {
        }
    }
}
