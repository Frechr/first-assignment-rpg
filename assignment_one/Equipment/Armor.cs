﻿using FirstAssignment_RPG.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Equipment
{
    public enum ArmorType
    {
        cloth, leather, mail, plate, DEFAULT
    }
    public class Armor : Item
    {
        //Defaults for instantiating when client is not providing arguments.
        public const string DEFAULT_NAME = "armor_default";
        public const int DEFAULT_LEVEL = 1;
        public const Slot DEFAULT_SLOT = Slot.DEFAULT;
        public const ArmorType DEFAULT_ARMOR_TYPE = ArmorType.DEFAULT;
        public HeroAttribute DEFAULT_ARMOR_ATTRIBUTE = HeroAttribute.DEFAULT;

        public ArmorType ArmorType { get; set; }
        public HeroAttribute ArmorAttribute { get; set; }
        public Armor(string name, int requiredLevel, Slot slot, ArmorType armorType, HeroAttribute armorAttribute) : base(name, requiredLevel, slot)
        {
            ArgumentNullException.ThrowIfNull(name);
            ArgumentNullException.ThrowIfNull(requiredLevel);
            ArgumentNullException.ThrowIfNull(slot);
            ArgumentNullException.ThrowIfNull(armorType);
            ArgumentNullException.ThrowIfNull(armorAttribute);
            ArmorType = armorType;
            ArmorAttribute = armorAttribute;
        }

        //Helper methods for easier construction of armor pieces
        public static Armor CreateHelmet(ArmorType armorType = ArmorType.DEFAULT, string name = "armor_helmet_default", HeroAttribute armorAttributes = null, int requiredLevel = DEFAULT_LEVEL)
        {
            return new Armor(name, requiredLevel, Slot.head, armorType, armorAttributes ?? HeroAttribute.DEFAULT); //Had to put the default value here as it is not a compile time constant...
        }
        public static Armor CreateBody(ArmorType armorType = ArmorType.DEFAULT, string name = "armor_body_default", HeroAttribute armorAttributes = null, int requiredLevel = DEFAULT_LEVEL)
        {
            return new Armor(name, requiredLevel, Slot.body, armorType, armorAttributes ?? HeroAttribute.DEFAULT); //Had to put the default value here as it is not a compile time constant...

        }
        public static Armor CreateLegs(ArmorType armorType = ArmorType.DEFAULT, string name = "armor_legs_default", HeroAttribute armorAttributes = null, int requiredLevel = DEFAULT_LEVEL)
        {
            return new Armor(name, requiredLevel, Slot.legs, armorType, armorAttributes ?? HeroAttribute.DEFAULT); //Had to put the default value here as it is not a compile time constant...
        }
    }
}
