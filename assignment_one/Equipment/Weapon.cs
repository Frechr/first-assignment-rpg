﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Equipment
{
    public enum WeaponType
    {
        axe, bow, dagger, hammer, staff, sword, wand, DEFAULT
    }
    public class Weapon : Item
    {
        //Defaults for empty weapon constructor
        public const string DEFAULT_NAME = "weapon_default";
        public const int DEFAULT_LEVEL = 1;
        public const int DEFAULT_WEAPON_DAMAGE = 5;

        public Weapon() : base(DEFAULT_NAME, DEFAULT_LEVEL, Slot.weapon)
        {
            WeaponType = WeaponType.DEFAULT;
            WeaponDamage = DEFAULT_WEAPON_DAMAGE;
        }

        public Weapon(string name, int requiredLevel, WeaponType weaponType, int weaponDamage) : base(name, requiredLevel, Slot.weapon)
        {
            ArgumentNullException.ThrowIfNull(name);
            ArgumentNullException.ThrowIfNull(requiredLevel);
            ArgumentNullException.ThrowIfNull(weaponType);
            ArgumentNullException.ThrowIfNull(weaponDamage);
            WeaponType = weaponType;
            WeaponDamage = weaponDamage;
        }

        public WeaponType WeaponType { get; set; }
        public int WeaponDamage { get; set; }

        //Helper methods for generating weapons
        public static Weapon CreateSword(string name = "weapon_sword_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.sword, weaponDamage);
        }
        public static Weapon CreateAxe(string name = "weapon_axe_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.axe, weaponDamage);
        }

        public static Weapon CreateBow(string name = "weapon_bow_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.bow, weaponDamage);
        }
        public static Weapon CreateDagger(string name = "weapon_dagger_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.dagger, weaponDamage);
        }
        public static Weapon CreateHammer(string name = "weapon_hammer_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.hammer, weaponDamage);
        }
        public static Weapon CreateStaff(string name = "weapon_staff_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.staff, weaponDamage);
        }
        public static Weapon CreateWand(string name = "weapon_wand_default", int requiredLevel = DEFAULT_LEVEL, int weaponDamage = DEFAULT_WEAPON_DAMAGE)
        {
            return new Weapon(name, requiredLevel, WeaponType.wand, weaponDamage);
        }
    }
}
