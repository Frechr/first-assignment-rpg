﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Equipment
{
    public enum Slot
    {
        weapon, head, body, legs, DEFAULT
    }
    public abstract class Item
    {
        // Defaults for empty constructor
        private const string DEFAULT_NAME = "item_default";
        private const int DEFAULT_LEVEL = 1;
        private const Slot DEFAULT_SLOT = Slot.DEFAULT;
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot { get; set; }
        protected Item(string name = DEFAULT_NAME, int requiredLevel = DEFAULT_LEVEL, Slot slot = Slot.DEFAULT)
        {
            Name = name ?? DEFAULT_NAME;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
    }
}
