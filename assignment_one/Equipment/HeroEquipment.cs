﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Exceptions;
using FirstAssignment_RPG.Hero;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Equipment
{
    public class HeroEquipment
    {
        Dictionary<Slot, Item> equipment { get; set; } = new Dictionary<Slot, Item>();

        public HeroEquipment()
        {
            //Using a dictionary to denote slots for each item
            equipment.Add(Slot.weapon, new EmptyItem());
            equipment.Add(Slot.head, new EmptyItem());
            equipment.Add(Slot.body, new EmptyItem());
            equipment.Add(Slot.legs, new EmptyItem());
        }

        //Indexer such that slot items can be accessed through HeroEquipment by using brackets
        public Item this[Slot slot]
        {
            get
            {
                return equipment[slot];
            }
            set
            {
                equipment[slot] = value ?? new EmptyItem();
            }
        }

        public void Add(Item item)
        {
            ArgumentNullException.ThrowIfNull(item);
            if (equipment[item.Slot] is not EmptyItem)
            {
                Console.WriteLine("Item swapped.");
            }
            equipment[item.Slot] = item;
        }

        //Method that allows chaining of item additions
        private HeroEquipment ChainedAdd(Item item)
        {
            Add(item);
            return this;
        }

        public HeroAttribute GetArmorAttributesTotal()
        {
            var attributesTotal = new HeroAttribute(0, 0, 0);
            if (equipment[Slot.head] is not EmptyItem)
            {
                attributesTotal.Add(((Armor)equipment[Slot.head]).ArmorAttribute);
            }
            if (equipment[Slot.body] is not EmptyItem)
            {
                attributesTotal.Add(((Armor)equipment[Slot.body]).ArmorAttribute);
            }
            if (equipment[Slot.legs] is not EmptyItem)
            {
                attributesTotal.Add(((Armor)equipment[Slot.legs]).ArmorAttribute);
            }
            return attributesTotal;
        }

        public int GetWeaponDamage()
        {
            if (equipment[Slot.weapon] is EmptyItem)
            {
                return 1; //Damage without weapons
            }
            return ((Weapon)equipment[Slot.weapon]).WeaponDamage; //Casting item to weapon so that the weapondamage property can be accessed
        }

        //Helper method for generating a full loadout for the warrior class
        public static HeroEquipment CreateWarriorBeginnerLoadout()
        {
            var beginnerSword = Weapon.CreateSword("Wooden Sword");
            var beginnerHelmet = Armor.CreateHelmet(ArmorType.mail, "Rusty Mail Coif", HeroAttribute.CreateBeginnerArmorAttributes<Warrior>());
            var beginnerBody = Armor.CreateBody(ArmorType.mail, "Rusty Mail Body", HeroAttribute.CreateBeginnerArmorAttributes<Warrior>());
            var beginnerLegs = Armor.CreateLegs(ArmorType.mail, "Rusty Mail Legs", HeroAttribute.CreateBeginnerArmorAttributes<Warrior>());
            //Chaining in action
            HeroEquipment warriorBeginnerLoadout = new HeroEquipment()
                .ChainedAdd(beginnerSword)
                .ChainedAdd(beginnerHelmet)
                .ChainedAdd(beginnerBody)
                .ChainedAdd(beginnerLegs);

            return warriorBeginnerLoadout;
        }
    }
}
