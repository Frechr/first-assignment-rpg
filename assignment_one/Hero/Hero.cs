﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;
using FirstAssignment_RPG.Exceptions;
using System.ComponentModel;

namespace FirstAssignment_RPG.Hero
{
    public abstract class Hero
    {

        public string Name { get; set; }
        public int Level { get; set; }
        public LevelAttributes LevelAttributes { get; set; }
        public HeroEquipment Equipment { get; set; } = new HeroEquipment();
        public List<WeaponType> ValidWeaponTypes { get; set; }
        public List<ArmorType> ValidArmorTypes { get; set; }

        protected Hero()
        {
            Name = string.Empty;
            Level = 1;
        }
        protected Hero(string name)
        {
            Name = name;
            Level = 1;
        }
        protected Hero(string name, HeroEquipment loadout)
        {
            Name = name;
            Equipment = loadout;
            Level = 1;
        }
        public void LevelUp()
        {
            Level++;
            LevelAttributes.IncreaseStats();
        }

        public void Equip(Armor armor)
        {
            ArgumentNullException.ThrowIfNull(armor);
            if (ValidArmorTypes.Contains(armor.ArmorType))
            {
                if (Level < armor.RequiredLevel)
                {
                    throw new LevelNotMetException("You don't have a high enough level to equip this.");
                }
                Equipment.Add(armor);
            }
            else
            {
                throw new InvalidArmorException("The armor type is not valid for this class.");
            }
        }

        public void Equip(Weapon weapon)
        {
            ArgumentNullException.ThrowIfNull(weapon);
            if (ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                if (Level < weapon.RequiredLevel)
                {
                    throw new LevelNotMetException("You don't have a high enough level to wield this.");
                }
                Equipment.Add(weapon);
            }
            else
            {
                throw new InvalidWeaponException("The weapon type is not valid for this class.");
            }
        }

        public string Display()
        {
            var attributes = TotalAttributes();
            return "Name: " + Name + ", Class: " + TypeDescriptor.GetClassName(this) + ", Level: " + Level + ", Strength: " + attributes.Strength + ", Dexterity: " + attributes.Dexterity + ", Intelligence: " + attributes.Intelligence + ", Damage: " + Damage();
        }

        public HeroAttribute TotalAttributes()
        {
            var attributesTotal = LevelAttributes.GetCurrentAttributes();
            attributesTotal.Add(Equipment.GetArmorAttributesTotal());
            return attributesTotal;
        }
        abstract public int Damage();
        //Helper method for testing, maybe this can be moved to the test classes by using extension methods?
        abstract public int DamagingAttributeValueTotal();
    }
}
