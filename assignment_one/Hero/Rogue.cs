﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Hero
{
    public class Rogue : Hero
    {
        public Rogue() : base("rogue_default")
        {
            LevelAttributes = new RogueLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.dagger, WeaponType.sword };
            ValidArmorTypes = new List<ArmorType> { ArmorType.leather, ArmorType.mail };
        }
        public Rogue(string name = "default") : base(name)
        {
            LevelAttributes = new RogueLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.dagger, WeaponType.sword };
            ValidArmorTypes = new List<ArmorType> { ArmorType.leather, ArmorType.mail };
        }
        public override int Damage()
        {
            return Equipment.GetWeaponDamage() * (1 + (TotalAttributes().Dexterity / 100));
        }
        public override int DamagingAttributeValueTotal()
        {
            return TotalAttributes().Dexterity;
        }
    }
}
