﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPG.Hero
{
    public class Warrior : Hero
    {
        public Warrior() : base("warrior_default")
        {
            LevelAttributes = new WarriorLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.axe, WeaponType.hammer, WeaponType.sword };
            ValidArmorTypes = new List<ArmorType> { ArmorType.mail, ArmorType.plate };
        }
        public Warrior(string name = "default") : base(name)
        {
            LevelAttributes = new WarriorLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.axe, WeaponType.hammer, WeaponType.sword };
            ValidArmorTypes = new List<ArmorType> { ArmorType.mail, ArmorType.plate };
        }

        public override int Damage()
        {
            return Equipment.GetWeaponDamage() * (1 + (TotalAttributes().Strength / 100));
        }
        public override int DamagingAttributeValueTotal()
        {
            return TotalAttributes().Strength;
        }
    }
}
