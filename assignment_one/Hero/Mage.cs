﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FirstAssignment_RPG.Hero
{
    public class Mage : Hero
    {
        public Mage() : base("mage_default")
        {
            LevelAttributes = new MageLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.staff, WeaponType.wand };
            ValidArmorTypes = new List<ArmorType> { ArmorType.cloth };
        }
        public Mage(string name = "default") : base(name)
        {
            LevelAttributes = new MageLevelAttributes();
            ValidWeaponTypes = new List<WeaponType> { WeaponType.staff, WeaponType.wand };
            ValidArmorTypes = new List<ArmorType> { ArmorType.cloth };
        }
        public override int Damage()
        {
            return Equipment.GetWeaponDamage() * (1 + (TotalAttributes().Intelligence / 100));
        }

        public override int DamagingAttributeValueTotal()
        {
            return TotalAttributes().Intelligence;
        }
    }
}
