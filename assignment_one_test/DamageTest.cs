﻿using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;
using FirstAssignment_RPG.Hero;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPGTest
{
    public class DamageTest
    {
        [Fact]
        void Damage_NoWeaponEquipped_ShouldDamageOneHP()
        {
            Hero hero = new Warrior();
            var expectedDamage = 1;

            var calculatedDamage = hero.Damage();

            Assert.Equal(expectedDamage, calculatedDamage);
        }

        [Fact]
        void Damage_WeaponEquipped_ShouldDamageAccordingToFormula()
        {
            var hero = new Warrior();
            var weapon = Weapon.CreateSword("Rusty Iron Sword", 1, 5);
            hero.Equip(weapon);
            var damageExpected = (int weaponDamage, int damagingAttribute) => weaponDamage * ( 1 + damagingAttribute/100);

            var calculatedDamage = hero.Damage();

            Assert.Equal(damageExpected(weapon.WeaponDamage, hero.DamagingAttributeValueTotal()), calculatedDamage);
        }
        [Fact]
        void Damage_EquipWeaponThenEquipAnotherWeapon_ShouldReturnCurrentAttributesPlusArmorAttributes()
        {
            var hero = new Warrior();
            var weaponOne = Weapon.CreateSword("Rusty Iron Sword", 1, 5);
            var weaponTwo = Weapon.CreateSword("Chipped Iron Sword", 1, 10);
            hero.Equip(weaponOne);
            hero.Equip(weaponTwo);
            var weaponDamage = weaponTwo.WeaponDamage;
            var damagingAttribute = hero.DamagingAttributeValueTotal();
            var damageExpected = (int weaponDamage, int damagingAttribute) => weaponDamage * (1 + damagingAttribute / 100);

            var calculatedDamage = hero.Damage();

            Assert.Equal(damageExpected(weaponDamage, damagingAttribute), calculatedDamage);
        }
        [Fact]
        void Damage_WeaponAndArmorEquipped_ShouldHaveTheLastArmorPieceEquipped()
        {
            var hero = new Warrior();
            var loadout = HeroEquipment.CreateWarriorBeginnerLoadout();
            var weaponDamage = ((Weapon)loadout[Slot.weapon]).WeaponDamage;
            var damagingAttribute = loadout.GetArmorAttributesTotal().Strength;
            var damageExpected = (int weaponDamage, int damagingAttribute) => weaponDamage * (1 + damagingAttribute / 100);
            hero.Equipment = loadout;

            var actualDamage = hero.Damage();

            Assert.Equal(damageExpected(weaponDamage,damagingAttribute), actualDamage);
        }
    }
}
