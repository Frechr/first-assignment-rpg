﻿using FirstAssignment_RPG.Hero;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FirstAssignment_RPGTest
{
    public class DisplayStateTest
    {
        [Fact]
        public void Display_DisplayHeroState_ShouldDisplayCorrectly()
        {
            var hero = new Warrior();
            var name = "warrior_default";
            var level = 1;
            var attributes = hero.TotalAttributes();
            var damage = hero.Damage();
            var expectedDisplayText =
                "Name: " + name +
                ", Class: " + TypeDescriptor.GetClassName(hero) +
                ", Level: " + level +
                ", Strength: " + attributes.Strength +
                ", Dexterity: " + attributes.Dexterity +
                ", Intelligence: " + attributes.Intelligence +
                ", Damage: " + damage;

            var actualDisplayText = hero.Display();

            Assert.Equal(actualDisplayText, expectedDisplayText);
        }
    }
}
