using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Hero;
using FirstAssignment_RPG.Attributes;
using System.Xml.Linq;

namespace FirstAssignment_RPGTest
{
    public class HeroTest
    {
        // -----------------------------------------------------------------------------------
        // Name checks
        // -----------------------------------------------------------------------------------
        [Fact]
        public void WarriorConstructor_InstantiateWarrior_WarriorShouldBeNamed()
        {
            //Allocate
            const string expectedName = "Trond";
            
            //Act
            Hero hero = new Warrior(expectedName); 

            //Assert
            Assert.Equal(expectedName, hero.Name);
        }

        [Fact]
        public void MageConstructor_InstantiateMage_MageShouldBeNamed()
        {
            //Allocate
            const string expectedName = "Trond";

            //Act
            Hero hero = new Mage(expectedName);

            //Assert
            Assert.Equal(expectedName, hero.Name);
        }

        [Fact]
        public void RogueConstructor_InstantiateRogue_RogueShouldBeNamed()
        {
            //Allocate
            const string expectedName = "Trond";

            //Act
            Hero hero = new Rogue(expectedName);

            //Assert
            Assert.Equal(expectedName, hero.Name);
        }

        [Fact]
        public void RangerConstructor_InstantiateRanger_RangerShouldBeNamed()
        {
            //Allocate
            const string expectedName = "Trond";

            //Act
            Hero hero = new Ranger(expectedName);

            //Assert
            Assert.Equal(expectedName, hero.Name);
        }

        // -----------------------------------------------------------------------------------
        // Level checks
        // -----------------------------------------------------------------------------------
        [Fact]
        public void WarriorConstructor_InstantiateWarrior_WarriorShouldBeLevelOne()
        {
            //Allocate
            const int expectedLevel = 1;

            //Act
            Hero hero = new Warrior();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void MageConstructor_InstantiateMage_MageShouldBeLevelOne()
        {
            //Allocate
            const int expectedLevel = 1;

            //Act
            Hero hero = new Mage();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void RogueConstructor_InstantiateRogue_RogueShouldBeLevelOne()
        {
            //Allocate
            const int expectedLevel = 1;

            //Act
            Hero hero = new Rogue();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void RangerConstructor_InstantiateRanger_RangerShouldBeLevelOne()
        {
            //Allocate
            const int expectedLevel = 1;

            //Act
            Hero hero = new Ranger();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        // -----------------------------------------------------------------------------------
        // Attribute checks
        // -----------------------------------------------------------------------------------
        [Fact]
        public void WarriorConstructor_InstantiateWarrior_WarriorShouldHaveWarriorAttributes()
        {
            //Allocate + act
            Hero warrior = new Warrior();
            LevelAttributes expectedWarriorLevelAttribute = new WarriorLevelAttributes();

            //Assert
            Assert.True(warrior.LevelAttributes.Equals(expectedWarriorLevelAttribute));
        }

        [Fact]
        public void MageConstructor_InstantiateMage_MageShouldHaveMageAttributes()
        {
            //Allocate + act
            Hero mage = new Mage();
            LevelAttributes expectedMageLevelAttribute = new MageLevelAttributes();

            //Assert
            Assert.Equal(expectedMageLevelAttribute, mage.LevelAttributes);
        }

        [Fact]
        public void RogueConstructor_InstantiateRogue_RogueShouldHaveRogueAttributes()
        {
            //Allocate + act
            Hero rogue = new Rogue();
            LevelAttributes expectedRogueLevelAttribute = new RogueLevelAttributes();

            //Assert
            Assert.True(rogue.LevelAttributes.Equals(expectedRogueLevelAttribute));
        }

        [Fact]
        public void RangerConstructor_InstantiateRanger_RangerShouldHaveRangerAttributes()
        {
            //Allocate + act
            Hero ranger = new Ranger();
            LevelAttributes expectedRangerLevelAttribute = new RangerLevelAttributes();

            //Assert
            Assert.True(ranger.LevelAttributes.Equals(expectedRangerLevelAttribute));
        }

        // -----------------------------------------------------------------------------------
        // Attribute increase checks
        // -----------------------------------------------------------------------------------
        [Fact]
        public void WarriorLevelUp_LevelUpWarrior_WarriorShouldBeLeveledUpByWarriorAttribute()
        {
            //Allocate
            Hero hero = new Warrior();

            WarriorLevelAttributes warriorAttributes = new WarriorLevelAttributes();
            HeroAttribute warriorsStartAttributes = warriorAttributes.GetCurrentAttributes();
            warriorsStartAttributes.Add(warriorAttributes.GetLevelAttributes());
            HeroAttribute expectedAttributeResult = warriorsStartAttributes;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedAttributeResult, hero.LevelAttributes.GetCurrentAttributes());
        }

        [Fact]
        public void MageLevelUp_LevelUpMage_MageShouldBeLeveledUpByMageAttribute()
        {
            //Allocate
            Hero hero = new Mage();

            MageLevelAttributes mageAttributes = new MageLevelAttributes();
            HeroAttribute magesStartAttributes = mageAttributes.GetCurrentAttributes();
            magesStartAttributes.Add(mageAttributes.GetLevelAttributes());
            HeroAttribute expectedAttributeResult = magesStartAttributes;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedAttributeResult, hero.LevelAttributes.GetCurrentAttributes());
        }

        [Fact]
        public void RogueLevelUp_LevelUpRogue_RogueShouldBeLeveledUpByRogueAttribute()
        {
            //Allocate
            Hero hero = new Rogue();

            RogueLevelAttributes rogueAttributes = new RogueLevelAttributes();
            HeroAttribute roguesStartAttributes = rogueAttributes.GetCurrentAttributes();
            roguesStartAttributes.Add(rogueAttributes.GetLevelAttributes());
            HeroAttribute expectedAttributeResult = roguesStartAttributes;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedAttributeResult, hero.LevelAttributes.GetCurrentAttributes());
        }

        [Fact]
        public void RangerLevelUp_LevelUpRanger_RangerShouldBeLeveledUpByRangerAttribute()
        {
            //Allocate
            Hero hero = new Ranger();

            RangerLevelAttributes rangerAttributes = new RangerLevelAttributes();
            HeroAttribute rangersStartAttributes = rangerAttributes.GetCurrentAttributes();
            rangersStartAttributes.Add(rangerAttributes.GetLevelAttributes());
            HeroAttribute expectedAttributeResult = rangersStartAttributes;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedAttributeResult, hero.LevelAttributes.GetCurrentAttributes());
        }

        // -----------------------------------------------------------------------------------
        // Level increase checks
        // -----------------------------------------------------------------------------------
        [Fact]
        public void WarriorLevelUp_LevelUpWarrior_WarriorLevelShouldBeTwo()
        {
            //Allocate
            Hero hero = new Warrior();
            var expectedLevel = 2;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void MageLevelUp_LevelUpMage_MageLevelShouldBeTwo()
        {
            //Allocate
            Hero hero = new Mage();
            var expectedLevel = 2;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void RogueLevelUp_LevelUpRogue_RogueLevelShouldBeTwo()
        {
            //Allocate
            Hero hero = new Rogue();
            var expectedLevel = 2;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }

        [Fact]
        public void RangerLevelUp_LevelUpRanger_RangerLevelShouldBeTwo()
        {
            //Allocate
            Hero hero = new Ranger();
            var expectedLevel = 2;

            //Act
            hero.LevelUp();

            //Assert
            Assert.Equal(expectedLevel, hero.Level);
        }
    }
}