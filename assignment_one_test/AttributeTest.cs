﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xunit;
using FirstAssignment_RPG.Hero;
using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;

namespace FirstAssignment_RPGTest
{
    public class AttributeTest
    {
        [Fact]
        void TotalAttributes_HeroWithoutArmor_ShouldReturnCurrentLevelAttributes()
        {
            Hero hero = new Warrior();
            var initialAttributes = hero.LevelAttributes.GetCurrentAttributes();

            var calculatedAttributes = hero.TotalAttributes();

            Assert.Equal(initialAttributes, calculatedAttributes);
        }

        [Fact]
        void TotalAttributes_HeroWithOneArmor_ShouldReturnCurrentAttributesPlusArmorAttributes()
        {
            var hero = new Warrior();
            var initialAttributes = hero.LevelAttributes.GetCurrentAttributes();
            var armorAttributes = HeroAttribute.CreateBeginnerArmorAttributes<Warrior>();
            var expectedAttributes = initialAttributes;
            expectedAttributes.Add(armorAttributes);
            var armor = Armor.CreateBody(ArmorType.mail, "Rusty Chain Body", armorAttributes);
            hero.Equip(armor);

            var calculatedAttributes = hero.TotalAttributes();

            Assert.Equal(expectedAttributes, calculatedAttributes);
        }
        [Fact]
        void TotalAttributes_HeroWithTwoArmor_ShouldReturnCurrentAttributesPlusArmorAttributes()
        {
            var hero = new Warrior();
            var initialAttributes = hero.LevelAttributes.GetCurrentAttributes();
            var armorAttributes = HeroAttribute.CreateBeginnerArmorAttributes<Warrior>();
            var expectedAttributes = initialAttributes;
            expectedAttributes.Add(armorAttributes);
            expectedAttributes.Add(armorAttributes);
            var armorOne = Armor.CreateBody(ArmorType.mail, "Rusty Chain Body", armorAttributes);
            var armorTwo = Armor.CreateLegs(ArmorType.mail, "Rusty Chain Legs", armorAttributes);
            hero.Equip(armorOne);
            hero.Equip(armorTwo);

            var calculatedAttributes = hero.TotalAttributes();

            Assert.Equal(expectedAttributes, calculatedAttributes);
        }
        [Fact]
        void Equip_EquipArmorThenEquipAnotherArmorInSameSlot_ShouldHaveTheLastArmorPieceEquipped()
        {
            var hero = new Warrior();
            var armorOne = Armor.CreateBody(ArmorType.mail, "Rusty Chain Body");
            var armorTwo = Armor.CreateBody(ArmorType.plate, "Rusty Plate Body");
            hero.Equip(armorOne);
            hero.Equip(armorTwo);

            var bodyArmorCurrentlyEquipped = hero.Equipment[Slot.body];

            Assert.Equal(armorTwo, bodyArmorCurrentlyEquipped);
        }
    }
}
