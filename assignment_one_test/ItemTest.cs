﻿using FirstAssignment_RPG.Equipment;
using FirstAssignment_RPG.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstAssignment_RPGTest
{
    public class ItemTest
    {
        //----------------------------------------------------------------------------------------------------
        // Weapon tests
        //----------------------------------------------------------------------------------------------------
        [Fact]
        public void ItemConstructor_WeaponInstantiation_ShouldHaveSameNameAsArgument()
        {
            //Allocate + act
            Item item = new Weapon(Weapon.DEFAULT_NAME, Weapon.DEFAULT_LEVEL, WeaponType.DEFAULT, Weapon.DEFAULT_WEAPON_DAMAGE);
            var expectedName = Weapon.DEFAULT_NAME;

            //Assert
            Assert.Equal(expectedName, item.Name);
        }
        [Fact]
        public void ItemConstructor_WeaponInstantiation_ShouldHaveSameLevelAsArgument()
        {
            //Allocate + act
            Item item = new Weapon(Weapon.DEFAULT_NAME, Weapon.DEFAULT_LEVEL, WeaponType.DEFAULT, Weapon.DEFAULT_WEAPON_DAMAGE);
            var expectedLevel = Weapon.DEFAULT_LEVEL;

            //Assert
            Assert.Equal(expectedLevel, item.RequiredLevel);
        }
        [Fact]
        public void ItemConstructor_WeaponInstantiation_ShouldHaveSameTypeAsArgument()
        {
            //Allocate + act
            Item item = new Weapon(Weapon.DEFAULT_NAME, Weapon.DEFAULT_LEVEL, WeaponType.DEFAULT, Weapon.DEFAULT_WEAPON_DAMAGE);
            var expectedType = WeaponType.DEFAULT;

            //Assert
            Assert.Equal(expectedType, ((Weapon)item).WeaponType);
        }
        [Fact]
        public void ItemConstructor_WeaponInstantiation_ShouldHaveSameDamageAsArgument()
        {
            //Allocate + act
            Item item = new Weapon(Weapon.DEFAULT_NAME, Weapon.DEFAULT_LEVEL, WeaponType.DEFAULT, Weapon.DEFAULT_WEAPON_DAMAGE);
            var expectedDamage = Weapon.DEFAULT_WEAPON_DAMAGE;

            //Assert
            Assert.Equal(expectedDamage, ((Weapon)item).WeaponDamage);
        }

        //----------------------------------------------------------------------------------------------------
        // Armor tests
        //----------------------------------------------------------------------------------------------------
        [Fact]
        public void ItemConstructor_ArmorInstantiation_ShouldHaveSameNameAsArgument()
        {
            //Allocate + act
            Item item = new Armor(Armor.DEFAULT_NAME, Armor.DEFAULT_LEVEL, Slot.DEFAULT, ArmorType.DEFAULT,
                HeroAttribute.DEFAULT);
            var expectedName = Armor.DEFAULT_NAME;

            //Assert
            Assert.Equal(expectedName, item.Name);
        }
        [Fact]
        public void ItemConstructor_ArmorInstantiation_ShouldHaveSameLevelAsArgument()
        {
            //Allocate + act
            Item item = new Armor(Armor.DEFAULT_NAME, Armor.DEFAULT_LEVEL, Slot.DEFAULT, ArmorType.DEFAULT,
                HeroAttribute.DEFAULT);
            var expectedRequiredLevel = Armor.DEFAULT_LEVEL;

            //Assert
            Assert.Equal(expectedRequiredLevel, item.RequiredLevel);
        }

        [Fact]
        public void ItemConstructor_ArmorInstantiation_ShouldHaveSameSlotAsArgument()
        {
            //Allocate + act
            Item item = new Armor(Armor.DEFAULT_NAME, Armor.DEFAULT_LEVEL, Slot.DEFAULT, ArmorType.DEFAULT,
                HeroAttribute.DEFAULT);
            var expectedArmorSlot = Slot.DEFAULT;

            //Assert
            Assert.Equal(expectedArmorSlot, item.Slot);
        }

        [Fact]
        public void ItemConstructor_ArmorInstantiation_ShouldHaveSameTypeAsArgument()
        {
            //Allocate + act
            Item item = new Armor(Armor.DEFAULT_NAME, Armor.DEFAULT_LEVEL, Slot.DEFAULT, ArmorType.DEFAULT,
                HeroAttribute.DEFAULT);
            var expectedArmorType = Armor.DEFAULT_ARMOR_TYPE;

            //Assert
            Assert.Equal(expectedArmorType, ((Armor)item).ArmorType);
        }
        [Fact]
        public void ItemConstructor_ArmorInstantiation_ShouldHaveSameArmorAttributeAsArgument()
        {
            //Allocate + act
            Item item = new Armor(Armor.DEFAULT_NAME, Armor.DEFAULT_LEVEL, Slot.DEFAULT, ArmorType.DEFAULT,
                HeroAttribute.DEFAULT);
            var expectedArmorAttribute = HeroAttribute.DEFAULT;

            //Assert
            Assert.Equal(expectedArmorAttribute, ((Armor)item).ArmorAttribute);
        }
    }
}
