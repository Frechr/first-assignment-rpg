﻿using FirstAssignment_RPG.Hero;
using FirstAssignment_RPG.Exceptions;
using FirstAssignment_RPG.Attributes;
using FirstAssignment_RPG.Equipment;

namespace FirstAssignment_RPGTest
{
    public class EquipmentTest
    {

        [Fact]
        public void Equip_EquipsWeapon_ShouldBeAddedToHerosEquipment()
        {
            //Alloc
            Hero hero = new Warrior();
            Weapon hammer = Weapon.CreateHammer("Construction Hammer");

            //Act
            hero.Equip(hammer);

            //Assert
            Assert.True(hero.Equipment[Slot.weapon] == hammer);
        }

        [Fact]
        public void Equip_EquipsIncompatibleWeapon_ShouldThrowInvalidWeaponException()
        {
            //Alloc
            Hero hero = new Warrior();
            Weapon staff = Weapon.CreateStaff("Ice Staff");
            //Act + Assert
            Assert.Throws<InvalidWeaponException>(() => hero.Equip(staff));
        }


        [Fact]
        public void Equip_EquipsWeaponWithTooHighLevel_ShouldThrowLevelNotMetException()
        {
            //Alloc
            Hero hero = new Warrior();
            Weapon axe = Weapon.CreateAxe("Magma splitter", 55, 66);

            //Act + Assert
            Assert.Throws<LevelNotMetException>(() => hero.Equip(axe));
        }

        [Fact]
        public void Equip_EquipsArmor_ShouldBeAddedToHerosEquipment()
        {
            //Alloc
            Hero hero = new Warrior();
            Armor helmet = Armor.CreateHelmet(ArmorType.mail);

            //Act
            hero.Equip(helmet);

            //Assert
            Assert.Equal(helmet, hero.Equipment[Slot.head]);
        }
        
        [Fact]
        public void Equip_EquipsIncompatibleArmor_ShouldThrowInvalidArmorException()
        {
            //Alloc
            Hero hero = new Warrior();
            Armor helmet = Armor.CreateHelmet(ArmorType.cloth);

            //Act + Assert
            Assert.Throws<InvalidArmorException>(() => hero.Equip(helmet));
        }

        [Fact]
        public void Equip_EquipsArmorWithTooHighLevel_ShouldThrowLevelNotMetException()
        {
            //Alloc
            Hero hero = new Warrior();
            Armor helmet = Armor.CreateHelmet(ArmorType.plate, "Steel Plate Helmet", HeroAttribute.CreateIntermediateArmorAttributes<Warrior>(), 25);

            //Act + Assert
            Assert.Throws<LevelNotMetException>(() => hero.Equip(helmet));
        }
    }
}
